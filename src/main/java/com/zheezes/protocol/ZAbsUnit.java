package com.zheezes.protocol;

public abstract class ZAbsUnit {
	public abstract int length();

	public abstract int length(int len);

	public int parse(char[] chs) {
		return parse(chs, 0, chs.length);
	}

	public abstract int parse(char[] chs, int off, int len);

	public int pack(char[] chs) {
		return pack(chs, 0);
	}

	public abstract int pack(char[] chs, int off);
}
