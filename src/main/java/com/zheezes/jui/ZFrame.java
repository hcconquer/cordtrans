package com.zheezes.jui;

import java.util.HashMap;

import com.zheezes.util.ZCast;

public class ZFrame {
	private HashMap<Integer, Object> modules = new HashMap<Integer, Object>();
	
	protected void modput(int key, Object m) {
		modules.put(key, m);
	}

	protected Object modget(int key) {
		return modules.get(key);
	}
	
	protected <T> T modget(int key, Class<T> clazz) {
		return ZCast.cast(modules.get(key), clazz);
	}
}
