package com.zheezes.jui;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class ZTable {
	public static void setColAlign(JTable table, int col, int align) {
		DefaultTableCellRenderer cr = new DefaultTableCellRenderer();
		cr.setHorizontalAlignment(align);
		table.getColumnModel().getColumn(col).setCellRenderer(cr);
	}
	
	public static void hideColumn(JTable table, int col) {
		TableColumnModel tcm = table.getColumnModel();
		TableColumn tc = tcm.getColumn(col);
		tcm.removeColumn(tc);
	}
}
