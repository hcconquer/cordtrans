package com.zheezes.demo.lang;

/**
 * finally will effect loop break and function return value
 */
public class Finnally {
	/**
	 * print 0, 1, 2
	 * continue in finally will continue the loop
	 */
	@SuppressWarnings("finally")
	public static void testLoop() {
		for (int i = 0; i < 3; i++) {
			System.out.println(i);
			try {
				break;
			} catch (Exception e) {
				
			} finally {
				continue;
			}
		}
	}
	
	/**
	 * will return 1 which set in finally
	 * thought about principle of stack, return value 
	 * will save in the stack, so when the function and finally execute,
	 * return value will be changed 
	 */
	public static int testReturn() {
		int ret = 0;
		try {
			Integer.parseInt(null);
		} catch (NumberFormatException e) {
		} finally {
			ret = 1;
		}
		return ret;
	}
	
	public static void main(String[] args) {
		testLoop();
		System.out.printf("%d\n", testReturn());
	}
}
