package com.zheezes.demo.exam;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/*

Ross is an event organizer. He has received data regarding the participation of employees in two different events. Some employees have participated in only one event and others have participated in both events. Ross now needs to count the number of employees who have taken part in both events. The records received by Ross consist of employee ids, which are unique. Write a program that accepts the employee ids participating in each event (the first line relates to the first event and the second line relates to the second event). The program should print the number of common employee ids in both the events.

Suppose the following input is given to the program, where each line represents a different event:

1001,1002,1003,1004,1005
1106,1008,1005,1003,1016,1017,1112

Now the common employee ids are 1003 and 1005, so the program should give the output as:

2

**/

public class Uniq {
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}
	
	public static void main(String[] args) {
		String[] input = getInput(2);
		if (input == null) {
			return;
		}
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (String line : input) {
			String[] ss = line.split(",");
			for (String s: ss) {
				Integer num = null;
				try {
					num = map.get(s);
				} catch (Exception e) {
					continue;
				}
				if (num == null || num == 0) {
					map.put(s, 1);
				} else {
					map.put(s, num + 1);
				}
			}
		}
		int count = 0;
		for (Entry<String, Integer> e: map.entrySet()) {
			if (e.getValue() > 1) {
				count++;
			}
		}
		System.out.println(count);
	}
}
