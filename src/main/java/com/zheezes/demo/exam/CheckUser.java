package com.zheezes.demo.exam;

/**
 * 
 * 
 * Sam wants to select a username in order to register on a website.
 * 
 * The rules for selecting a username are:
 * 
 * 1. The minimum length of the username must be 5 characters and the maximum
 * may be 10. 2. It should contain at least one letter from A-Z 3. It should
 * contain at least one digit from 0-9 4. It should contain at least one
 * character from amongst @#*= 5. It should not contain any spaces
 * 
 * Write a program which accepts 4 usernames (one username per line) as input
 * and checks whether each of them satisfy the above mentioned conditions. If a
 * username satisfies the conditions, the program should print PASS (in
 * uppercase) If a username fails the conditions, the program should print FAIL
 * (in uppercase)
 * 
 * Suppose the following usernames are supplied to the program: 1234@a ABC3a#@
 * 1Ac@ ABC 3a#@
 * 
 * Then the output should be: FAIL PASS FAIL FAIL
 * 
 * IMPORTANT NOTES - READ CAREFULLY:
 * 
 * 1. Your solution should assume console input
 * 
 * 2. Your solution should contain class name as Main, as the solution will be
 * compiled as Main.java
 * 
 * @author hanchen
 * 
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckUser {
	private static Pattern[] userRegex = null;
	
	static {
		String[] rs = { "[a-z]+", "[A-Z]+", "[0-9]+", "[@#*=]+", "[^\\s]{5,10}"};
		userRegex = new Pattern[rs.length];
		for (int i = 0; i < rs.length; i++) {
			userRegex[i] = Pattern.compile(rs[i]);
		}
	}
	
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}

	public static boolean checkUserName(String username) {
		if (username == null || username.length() < 5 || username.length() > 10) {
			return false;
		}
		int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
		for (char c : username.toCharArray()) {
			if ('0' <= c && c <= '9') {
				n1++;
			} else if ('a' <= c && c <= 'z') {
				n2++;
			} else if ('A' <= c && c <= 'Z') {
				n3++;
			} else if (c == '@' || c == '#' || c == '*' || c == '=') {
				n4++;
			} else if (Character.isWhitespace(c)) {
				return false;
			}
		}
		if (n1 < 1 || n2 < 1 || n3 < 1 || n4 < 1) {
			return false;
		}

		return true;
	}

	public static boolean checkUserNameByPattern(String username) {
		for (Pattern p: userRegex) {
			Matcher match = p.matcher(username);
			if (!match.find()) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		String[] us = getInput(4);
		for (String s : us) {
			if (checkUserNameByPattern(s)) {
				System.out.println("PASS");
			} else {
				System.out.println("FAIL");
			}
		}
	}
}
