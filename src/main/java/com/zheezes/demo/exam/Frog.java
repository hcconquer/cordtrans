package com.zheezes.demo.exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
/*
Kermit, a frog hops in a particular way such that:
1. He hops 20cm in the first hop, 10cm in the second hop and 5cm in the third hop.
2. After three hops Kermit rests for a while and then again follows the same hopping pattern.

Calculate the total distance travelled by Kermit (in centimeters) for the provided number of hops. Exactly 4 numbers of hops will be provided to the program (one number per line) as per the below example.

Suppose the following number of hops is provided to the program:

4
6
3
5

Then the total distance covered should be displayed as follows:

55
70
35
65
**/

public class Frog {
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}
	
	public static void main(String[] args) {
		String[] input = getInput(4);
		if (input == null) {
			return;
		}
		int hops[] = { 20, 10, 5 };
		for (String s : input) {
			int num = Integer.parseInt(s);
			int sum = 0;
			for (int n = 0; n < num; n++) {
				sum += hops[n % hops.length];
			}
			System.out.println(sum);
		}
	}
}


