package com.zheezes.demo.exam;

/*
 * Write a program which will print the below structures according to the input provided to the program. The program should accept 3 inputs in the form of numbers between 1 and 9, both inclusive (one number per line) and then generate the corresponding structures based on the input.
 * Suppose the following sequence of numbers is supplied to the program:
 * 3
 * 2
 * 4

 * Then the output should be:
 *   1
 *  2 2
 * 3 3 3
 *  1
 * 2 2
 *    1
 *   2 2
 *  3 3 3
 * 4 4 4 4
**/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PrintTree {
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}
	
	private static void print(int size) {
		for (int r = 0; r < size; r++) {
			for (int c = 0; c < size - r - 1; c++) {
				System.out.print(" ");
			}
			for (int c = 0; c <= r; c++) {
				if (c < r) {
					System.out.printf("%d ", r + 1);
				} else {
					System.out.printf("%d", r + 1);
				}
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		String[] input = getInput(3);
		if (input == null) {
			return;
		}
		for (String s : input) {
			int size = Integer.parseInt(s);
			print(size);
		}
	}
}
