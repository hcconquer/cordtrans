package com.zheezes.demo.exam;
/**
 * 
 * Write a program that prints the numbers between 258 and 393 (both inclusive) which do not end with 5. 
 * The program should print the output so as to have one value per line. 
 * The output would therefore follow the below format:
 * 
 * value1
 * value2
 * value3
 * .
 * .
 * .
 * .
 * so on
 * 
 * @author hanchen
 * 
 */

public class NumNotEndFive {
	public static void main(String[] args) {
		for (int n = 258; n <= 393; n++) {
			int mod = n % 10;
			if (mod == 5) {
				continue;
			}
			System.out.println(n);
		}
	}
}
