package com.zheezes.demo.exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
/*
Write a program to calculate the distance travelled by a car at different time intervals. 
The initial velocity of the car is 36 km/hr and the constant acceleration is 5 m/s2.

The formula to calculate distance is:

Distance Travelled = u*t+((a*t*t)/2) where,
u = initial velocity of the car (36 km/hr)
a = acceleration of the car (5 m/s2)
t = time duration in seconds

The program should accept 2 time intervals as the input (one time interval per line) 
and print the distance travelled in meters by the car (one output per line).

Definitions:
------------
1 kilometer = 1000 meters
1 hour = 3600 seconds

Let us suppose following are the inputs supplied to the program

10
8

Then the output of the program will be

350
240
**/

public class Speed {
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}
	
	private static double calc(double u, double a, double t) {
		return u * t + ((a * t * t) / 2);
	}

	private static double calc(double t) {
		return calc(36 * 1000 / 3600, 5, t);
	}

	public static void main(String[] args) {
		String[] input = getInput(2);
		for (String s : input) {
			try {
				int t = Integer.parseInt(s);
				System.out.println((int) calc(t));
			} catch (NumberFormatException e) {
			}
		}
	}
}

