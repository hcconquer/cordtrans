package com.zheezes.demo.exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

/*
Write a program that accepts 10 student records (roll number and score) and prints them in decreasing order of scores. In case there are multiple records pertaining to the same student, the program should choose a single record containing the highest score. The program should be capable of accepting a multi-line input. Each subsequent line of input will contain a student record, that is, a roll number and a score (separated by a hyphen). The output should consist of the combination of roll number and corresponding score in decreasing order of score.

INPUT to program

1001-40
1002-50
1003-60
1002-80
1005-35
1005-55
1007-68
1009-99
1009-10
1004-89

OUTPUT from program

1009-99
1004-89
1002-80
1007-68
1003-60
1005-55
1001-40

**/

public class ScoreSort {
	private String no;
	private int score;
	
	private static String[] getInput(int num) {
		List<String> list =new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int count = 0;
			String s = null;
			while ((s = reader.readLine()) != null) {
				list.add(s);
				count++;
				if (count >= num) {
					break;
				}
			}
		} catch (IOException e) {
		}
		if (list.size() != num) {
			return null;
		}
		return list.toArray(new String[0]);
	}

	private int setValue(String line) {
		if (line == null) {
			return -1;
		}
		String[] vs = line.split("-");
		if (vs == null || vs.length < 2) {
			return -1;
		}
		no = vs[0];
		score = Integer.valueOf(vs[1]);
		return 0;
	}

	public static void main(String[] args) {
		List<ScoreSort> list = new ArrayList<ScoreSort>();
		String[] inputs = getInput(10);
		for (String s : inputs) {
			ScoreSort m = new ScoreSort();
			if (m.setValue(s) < 0) {
				continue;
			}
			list.add(m);
		}
		ScoreSort[] arr = list.toArray(new ScoreSort[0]);
		Arrays.sort(arr, new Comparator<ScoreSort>() {
			public int compare(ScoreSort m, ScoreSort n) {
				return m.score - n.score;
			}
		});
		TreeMap<String, ScoreSort> map = new TreeMap<String, ScoreSort>();
		for (ScoreSort m : arr) {
			map.put(m.no, m);
		}
		list.clear();
		for (Entry<String, ScoreSort> e : map.entrySet()) {
			ScoreSort m = e.getValue();
			list.add(m);
		}
		arr = list.toArray(new ScoreSort[0]);
		Arrays.sort(arr, new Comparator<ScoreSort>() {
			public int compare(ScoreSort m, ScoreSort n) {
				return n.score - m.score;
			}
		});
		for (ScoreSort m : arr) {
			System.out.printf("%s-%d\n", m.no, m.score);
		}
	}
}
