package com.zheezes.cordtrans;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zheezes.sql.ZColumn;
import com.zheezes.sql.ZDatabase;
import com.zheezes.sql.ZDatabaseMeta;
import com.zheezes.sql.ZTable;

public class CordTrans {
	private static final Logger logger = LoggerFactory.getLogger(CordTrans.class);
	
	private static final String CORD_TABLE_PREFIX = "Z";
	private static final String CORD_COLUMN_PREFIX = "Z";
	
	private static final String CORD_PRIMK_TABLE_NAME = "Z_PRIMARYKEY";
	private static final String CORD_PRIMK_COLUMN_ENT = "Z_ENT";
	private static final String CORD_PRIMK_COLUMN_TNAME = "Z_NAME";
	
	// it's origin core data order
	private static final String CORD_COLUMN_PK = "Z_PK";
	private static final String CORD_COLUMN_ENT = "Z_ENT";
	private static final String CORD_COLUMN_OPT = "Z_OPT";
	private static final String[] CORD_COLUMNS = new String[] { CORD_COLUMN_PK, CORD_COLUMN_ENT, CORD_COLUMN_OPT };
	private static final int CORD_OPT_VAL = 1;
	
	private String database_driver_src = "com.mysql.jdbc.Driver";
	private String database_url_src = "jdbc:mysql://localhost:3306/test";
	private String database_username_src = "";
	private String database_password_src = "";

	private String database_driver_dst = "org.sqlite.JDBC";
	private String database_url_dst = "jdbc:sqlite:test.sqlite";
	private String database_username_dst = "";
	private String database_password_dst = "";

	private boolean table_truncate_dst = true;
	// private boolean column_mismatch_ingore = true;
	
	private ZTableAscComparator TABLE_ASC_COMPARATOR = new ZTableAscComparator();
	private ZColumnAscComparator COLUMN_ASC_COMPARATOR = new ZColumnAscComparator();
	
	private class ZTableAscComparator implements Comparator<ZTable> {
		public int compare(ZTable o1, ZTable o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}
	
	// sort by column name but CROD_COLUMN should be at last
	private class ZColumnAscComparator implements Comparator<ZColumn> {		
		public int compare(ZColumn o1, ZColumn o2) {
			int i1 = Arrays.binarySearch(CORD_COLUMNS, o1.getName());
			int i2 = Arrays.binarySearch(CORD_COLUMNS, o2.getName());
			if ((i1 < 0) && (i2 < 0)) {
				return o1.getName().compareTo(o2.getName());
			} else {
				return i1 - i2;
			}
		}
	}
	
	static {
		Arrays.sort(CORD_COLUMNS);
	}
	
	private boolean keyeq(String k1, String k2) {
		k1 = k1.replace('_', '.');
		k2 = k2.replace('_', '.');
		return k1.equalsIgnoreCase(k2);
	}
	
	public int loadConfig(String path) {		
		InputStream in = getClass().getResourceAsStream(path);
		if (in == null) {
			return -1;
		}
		Properties config = new Properties();
		try {
			config.load(in);
		} catch (IOException e) {
			logger.error(e.getMessage());
			return -1;
		}
		
		String key, value;
		for (Entry<Object, Object> e: config.entrySet()) {
			key = (String) e.getKey();
			value = (String) e.getValue();
			if (keyeq(key, "database_driver_src")) {
				database_driver_src = value;
			} else if (keyeq(key, "database_url_src")) {
				database_url_src = value;
			} else if (keyeq(key, "database_username_src")) {
				database_username_src = value;
			} else if (keyeq(key, "database_password_src")) {
				database_password_src = value;
			} else if (keyeq(key, "database_driver_dst")) {
				database_driver_dst = value;
			} else if (keyeq(key, "database_url_dst")) {
				database_url_dst = value;
			} else if (keyeq(key, "database_username_dst")) {
				database_username_dst = value;
			} else if (keyeq(key, "database_password_dst")) {
				database_password_dst = value;
			} else if (keyeq(key, "table_truncate_dst")) {
				table_truncate_dst = Boolean.parseBoolean(value);
			} /*else if (keyeq(key, "column_mismatch_ingore")) {
				column_mismatch_ingore = Boolean.parseBoolean(value);
			}*/
		}
		
		return 0;
	}
	
	private String buildTruncatePrepStmtSql(ZDatabaseMeta dbmt, String table) {
		String sql = null;
		if (dbmt.getDatabase() == ZDatabaseMeta.DATABASE_SQLITE) {
			sql = String.format("DELETE FROM %s", table);
		} else {
			sql = String.format("TRUNCATE TABLE %s", table);
		}
		return sql;
	}
	
	private String buildRetrievePrepStmtSql(String table, List<ZColumn> columns) {
		StringBuilder colsb = new StringBuilder();
		for (ZColumn column: columns) {
			colsb.append(column.getName()).append(", ");
		}
		int idx = colsb.lastIndexOf(",");
		colsb.delete(idx, colsb.length());
		colsb.substring(0, idx);
		String sql = String.format("SELECT %s FROM %s", colsb.toString(), table);
		return sql;
	}
	
	private String buildCreatePrepStmtSql(String table, List<ZColumn> columns, int ent) {
		StringBuilder colsb = new StringBuilder();
		StringBuilder valsb = new StringBuilder();
		for (ZColumn column: columns) {
			colsb.append(column.getName()).append(", ");
			if (column.getName().equalsIgnoreCase(CORD_COLUMN_ENT)) {
				valsb.append(ent).append(", ");
			} else if (column.getName().equalsIgnoreCase(CORD_COLUMN_OPT)) {
				valsb.append(CORD_OPT_VAL).append(", ");
			} else {
				valsb.append("?").append(", ");
			}
		}
		int idx = -1;
		idx = colsb.lastIndexOf(",");
		colsb.delete(idx, colsb.length());
		idx = valsb.lastIndexOf(",");
		valsb.delete(idx, valsb.length());
		String sql = String.format("INSERT INTO %s(%s) VALUES(%s)", table, colsb.toString(), valsb.toString());
		return sql;
	}
	
	public int TruncateTable(Connection conn, String tab) throws SQLException {
		logger.debug(String.format("truncate table %s", tab));
		String sql = buildTruncatePrepStmtSql(ZDatabase.getMetaData(conn), tab);
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.executeUpdate();
		return 0;
	}
	
	private boolean match(ZColumn dcol, ZColumn scol) {
		boolean match = true;
		String dname = dcol.getName();
		String sname = scol.getName();
		if (dname.length() == sname.length()) {
			match = dname.equalsIgnoreCase(sname);
		} else if (dname.length() > sname.length()) {
			match = dname.equalsIgnoreCase(CORD_COLUMN_PREFIX + sname);
		} else {
			match = sname.equalsIgnoreCase(CORD_COLUMN_PREFIX + dname);
		}
		if (!match) {
			logger.error(String.format("column %s not match with %s", dcol.getName(), scol.getName()));
		}
		return match;
	}
	
	private int matchnum(List<ZColumn> dcols, List<ZColumn> scols) {
		int num = 0;
		for (int i = 0; i < scols.size(); i++) {
			ZColumn scol = scols.get(i);
			ZColumn dcol = dcols.get(i);
			boolean match = match(dcol, scol);
			if (!match) {
				break;
			}
			num++;
		}
		return num;
	}
	
	private String getModelName(String tab) {
		// int idx = tab.indexOf(CORD_COLUMN_PREFIX);
		if (tab.length() <= CORD_TABLE_PREFIX.length()) {
			return null;
		}
		if (!tab.startsWith(CORD_TABLE_PREFIX)) {
			return null;
		}
		String name = tab.substring(CORD_TABLE_PREFIX.length());
		return name;
	}
	
	private int getTableEnt(Connection conn, String tab) throws SQLException {
		int ent = -1, cnt = 0;
		String model = getModelName(tab);
		String sql = String.format("SELECT %s FROM %s WHERE upper(%s) = upper('%s')", 
				CORD_PRIMK_COLUMN_ENT, CORD_PRIMK_TABLE_NAME, CORD_PRIMK_COLUMN_TNAME, model);
		logger.debug(sql);
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			cnt++;
			if (cnt > 1) {
				logger.warn("too much result int ent");
				break;
			}
			ent = rs.getInt(CORD_COLUMN_ENT);
			if (ent <= 0) {
				logger.error(String.format("invalid %s value %d with table %s", 
						CORD_COLUMN_ENT, ent, tab));
			}
		}
		rs.close();
		if (cnt <= 0) {
			logger.warn(String.format("no %s value in %s with table %s",
					CORD_COLUMN_ENT, CORD_PRIMK_TABLE_NAME, tab));
		}
		return ent;
	}
	
	public int transTableData(Connection dconn, String dtab, Connection sconn, String stab) throws SQLException {
		List<ZColumn> scols = ZDatabase.getColumns(sconn, stab);
		List<ZColumn> dcols = ZDatabase.getColumns(dconn, dtab);
		Collections.sort(scols, COLUMN_ASC_COMPARATOR);
		Collections.sort(dcols, COLUMN_ASC_COMPARATOR);
		// int matchn = Math.min(dcols.size(), scols.size());
		int matchn = matchnum(dcols, scols);
		if (matchn < Math.min(dcols.size(), scols.size())) {
			logger.error(String.format("table %s not match with %s", dtab, stab));
			return -1;
		}
//		if (matchn != dcols.size()) {
//			if (!column_mismatch_ingore) {
//				return -1;
//			}	
//		}
		int dent = getTableEnt(dconn, dtab);
		logger.debug(String.format("table %s's ent is %d", dtab, dent));
		String ssql = buildRetrievePrepStmtSql(stab, scols);
		String dsql = buildCreatePrepStmtSql(dtab, dcols, dent);
		logger.debug(ssql);
		logger.debug(dsql);
		PreparedStatement sstmt = sconn.prepareStatement(ssql);
		PreparedStatement dstmt = dconn.prepareStatement(dsql);
		ResultSet srs = sstmt.executeQuery();
		while (srs.next()) {
			for (int i = 0; i < matchn; i++) {
				ZColumn scol = scols.get(i);
//				ZColumn dcol = dcols.get(i);
				if (scol.getDatatype() == Types.INTEGER) {
					Integer v = srs.getInt(i + 1);
					dstmt.setInt(i + 1, v);
				} else if (scol.getDatatype() == Types.VARCHAR) {
					String v = srs.getString(i + 1);
					dstmt.setString(i + 1, v);
				} else {
					logger.error(String.format("column %s %d can't resolve", scol.getName(), scol.getDatatype()));
				}
			}
			dstmt.executeUpdate();
		}
		srs.close();
		return 0;
	}
	
	public int transData(Connection dconn, Connection sconn) throws SQLException {
		int ret = 0;
		List<ZTable> stabs = ZDatabase.getTables(sconn);
		List<ZTable> dtabs = ZDatabase.getTables(dconn);
		Collections.sort(stabs, TABLE_ASC_COMPARATOR);
		Collections.sort(dtabs, TABLE_ASC_COMPARATOR);
		for (ZTable stab: stabs) {
			ZTable tab = new ZTable();
			tab.setName(String.format("%s%s", CORD_COLUMN_PREFIX, stab.getName()).toUpperCase());
			int di = Collections.binarySearch(dtabs, tab, TABLE_ASC_COMPARATOR);
			if (di < 0) {
				continue;
			}
			ZTable dtab = dtabs.get(di);
			if (table_truncate_dst) {
				ret = TruncateTable(dconn, dtab.getName());
				if (ret < 0) {
					return -1;
				}
			}
			ret = transTableData(dconn, dtab.getName(), sconn, stab.getName());
			logger.debug("TransTableData, ret: " + ret);
			if (ret < 0) {
				return -1;
			}
		}
		return 0;
	}
	
	private Connection getDatabaseConn(String driver, String url, String user, String password) throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, user, password);
		return conn;
	}
	
	/*
	 * strictly speaking, it has some problem, if one connection has problem, 
	 * need to close but not throw execption
	 */
	public int TransData() throws ClassNotFoundException, SQLException {
		int ret = 0;
		Connection sconn = null, dconn = null;
		try {
			sconn = getDatabaseConn(database_driver_src,
					database_url_src,
					database_username_src,
					database_password_src);
			dconn = getDatabaseConn(database_driver_dst,
					database_url_dst,
					database_username_dst,
					database_password_dst);
			if ((sconn == null) || (dconn == null)) {
				ret = -1;
			}
			if (ret == 0) {
				dconn.setAutoCommit(false);
				ret = transData(dconn, sconn);
				if (ret < 0) {
					logger.error(String.format("TransData fail, ret: %d", ret));
				} else {
					dconn.commit();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (sconn != null) {
				sconn.close();
			}
			if (dconn != null) {
				dconn.close();
			}
		}
		return ret;
	}
	
	public static void main(String[] args) {
		CordTrans cordTrans = new CordTrans();
		int ret = 0;
		ret = cordTrans.loadConfig("/config.properties");
		// logger.debug(String.format("loadConfig: %d", ret));
		try {
			ret = cordTrans.TransData();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (ret < 0) {
			logger.error("deal fail");
		}
	}
}
