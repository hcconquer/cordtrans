package com.zheezes.conv;

import com.zheezes.util.ZChar;

/**
 * @author hcconquer@gmail.com
 */
public class ZDecNumToChs implements ZINumToChs {
	/**
	 * Integer(12) --> char[]{'1', '2'}
	 */
	public static char[] convs(int num) {
		return ZChar.toDecs(num);
	}

	@Override
	public char[] conv(int num) {
		return convs(num);
	}

	public static void main(String[] args) {
		char[] chs = convs(12);
		System.out.printf("num: %s\n", new String(chs));
	}
}
