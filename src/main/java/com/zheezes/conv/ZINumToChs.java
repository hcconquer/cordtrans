package com.zheezes.conv;

/**
 * @author hcconquer@gmail.com
 */
public interface ZINumToChs {
	public char[] conv(int num);
}
