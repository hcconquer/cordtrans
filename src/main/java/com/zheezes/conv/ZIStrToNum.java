package com.zheezes.conv;

/**
 * @author hcconquer@gmail.com
 */
public interface ZIStrToNum {
	public Integer conv(String str);
}
