package com.zheezes.conv;

import com.zheezes.util.ZChar;

/**
 * @author hcconquer@gmail.com
 */
public class ZHexChsToNum implements ZIChsToNum {
	/**
	 * char[]{'1', '2'} --> Integer(0x12)
	 */
	public static Integer convs(char[] chs) {
		int num = ZChar.toHex(chs);
		return num < 0 ? null : num;
	}

	/**
	 * @see zheezes.conv.ZIChsToNum#conv(char[])
	 */
	@Override
	public Integer conv(char[] chs) {
		return convs(chs);
	}
	
	public static void main(String[] args) {
		int num = convs(new char[] { '1', 'a' });
		System.out.printf("num: %d\n", num);
	}
}
