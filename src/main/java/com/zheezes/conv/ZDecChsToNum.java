package com.zheezes.conv;

import com.zheezes.util.ZChar;

/**
 * @author hcconquer@gmail.com
 */
public class ZDecChsToNum implements ZIChsToNum {
	/**
	 * char[]{'1', '2'} --> Integer(12)
	 */
	public static Integer convs(char[] chs) {
		int num = ZChar.toDec(chs);
		return num < 0 ? null : num;
	}

	@Override
	public Integer conv(char[] chs) {
		return convs(chs);
	}

	public static void main(String[] args) {
		int num = convs(new char[] { '1', '2' });
		System.out.printf("num: %d\n", num);
	}
}
