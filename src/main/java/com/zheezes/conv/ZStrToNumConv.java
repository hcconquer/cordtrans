package com.zheezes.conv;

/**
 * @author hcconquer@gmail.com
 */
public interface ZStrToNumConv {
	public Integer conv(String str);
}
