package com.zheezes.conv;

import com.zheezes.util.ZChar;

/**
 * @author hcconquer@gmail.com
 */
public class ZHexNumToChs implements ZINumToChs {
	/**
	 * Integer(0x12) --> char[]{'1', '2'}
	 */
	public static char[] convs(int num) {
		return ZChar.toHexs(num);
	}

	/**
	 * @see zheezes.conv.ZIChsToNum#conv(char[])
	 */
	@Override
	public char[] conv(int num) {
		return convs(num);
	}
	
	public static void main(String[] args) {
		int num = 0x8f;
		char[] chs = convs(num);
		System.out.printf("%c %c\n", chs[0], chs[1]);
	}
}
