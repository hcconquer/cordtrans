package com.zheezes.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ZDatabase {
	public static ZDatabaseMeta getMetaData(Connection conn) throws SQLException {
		ZDatabaseMeta meta = new ZDatabaseMeta();
		DatabaseMetaData md = conn.getMetaData();
		meta.setName(md.getDatabaseProductName());
		if (md.getDatabaseProductName().equals("SQLite")) {
			meta.setDatabase(ZDatabaseMeta.DATABASE_SQLITE);
		}
		return meta;
	}
	
	public static List<ZTable> getTables(Connection conn) throws SQLException {
		List<ZTable> tables = new ArrayList<ZTable>();
		DatabaseMetaData md = conn.getMetaData();
		ResultSet rs = md.getTables(null, "%", "%", new String[] { "TABLE" });
		while (rs.next()) {
			ZTable table = new ZTable();
			table.setName(rs.getString(ZTable.TABLE_NAME));
			tables.add(table);
		}
		return tables;
	}
	
	public static List<String> getTableNames(Connection conn) throws SQLException {
		List<String> tables = new ArrayList<String>();
		List<ZTable> ts = getTables(conn);
		for (ZTable t: ts) {
			tables.add(t.getName());
		}
		return tables;
	}
	
	public static List<ZColumn> getColumns(Connection conn, String table) throws SQLException {
		List<ZColumn> columns = new ArrayList<ZColumn>();
		DatabaseMetaData md = conn.getMetaData();
		ResultSet rs = md.getColumns(null, "%", table, "%");
		while (rs.next()) {
			ZColumn column = new ZColumn();
			column.setName(rs.getString(ZColumn.COLUMN_NAME));
			column.setDatatype(rs.getInt(ZColumn.DATA_TYPE));
			column.setTypename(rs.getString(ZColumn.TYPE_NAME));
			columns.add(column);
		}
		return columns;
	}
	
	public static List<String> getColumnNames(Connection conn, String table) throws SQLException {
		List<String> columns = new ArrayList<String>();
		List<ZColumn> cs = getColumns(conn, table);
		for (ZColumn c: cs) {
			columns.add(c.getName());
		}
		return columns;
	}
}
