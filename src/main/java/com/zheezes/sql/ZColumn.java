package com.zheezes.sql;

/**
 * reference http://www.herongyang.com/JDBC/sqljdbc-jar-Column-List.html
 */
public class ZColumn {
	public static final String TABLE_CAT = "TABLE_CAT";
	public static final String COLUMN_NAME = "COLUMN_NAME";
	public static final String TYPE_NAME = "TYPE_NAME";
	public static final String DATA_TYPE = "DATA_TYPE";
	
	/** TABLE_CAT String => table catalog (may be null) */
	private String catalog;
	/** TABLE_SCHEM String => table schema (may be null) */
	private String schema;
	/** TABLE_NAME String => table name */
	private String table;
	/** COLUMN_NAME String => column name */
	private String name;
	/** DATA_TYPE int => SQL type from {@link java.sql.Types} */
	private int datatype;
	/** TYPE_NAME String => Data source dependent type name, for a UDT the type name is fully qualified */
	private String typename;
	/** COLUMN_SIZE int => column size */
	private int size;
	/** DECIMAL_DIGITS int => the number of fractional digits. Null is returned for data types where DECIMAL_DIGITS is not applicable. */
	private int digits;
	/** NULLABLE int => is NULL allowed */
	private int nullable;

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * {@link java.sql.Types}
	 */
	public int getDatatype() {
		return datatype;
	}

	public void setDatatype(int datatype) {
		this.datatype = datatype;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getDigits() {
		return digits;
	}

	public void setDigits(int digits) {
		this.digits = digits;
	}

	public int getNullable() {
		return nullable;
	}

	public void setNullable(int nullable) {
		this.nullable = nullable;
	}
}
