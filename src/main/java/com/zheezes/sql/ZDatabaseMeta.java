package com.zheezes.sql;

public class ZDatabaseMeta {
	public static final int DATABASE_SQLITE = 1;
	
	private String name;
	private int database;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDatabase() {
		return database;
	}

	public void setDatabase(int database) {
		this.database = database;
	}
}
