package com.zheezes.util;

/**
 * @author hcconquer@gmail.com
 */
public abstract class ZThread extends Thread {
	public static int ARG_MAX = 32; // need to allow sub-class to extends

	protected Object[] args = new Object[ARG_MAX];

	public ZThread() {
		this(new Object[] {});
	}

	public ZThread(Object arg0) {
		this(new Object[] { arg0 });
	}

	public ZThread(Object arg0, Object arg1) {
		this(new Object[] { arg0, arg1 });
	}

	public ZThread(Object arg0, Object arg1, Object arg2) {
		this(new Object[] { arg0, arg1, arg2 });
	}

	public ZThread(Object arg0, Object arg1, Object arg2, Object arg3) {
		this(new Object[] { arg0, arg1, arg2, arg3 });
	}

	public ZThread(Object arg0, Object arg1, Object arg2, Object arg3,
			Object arg4) {
		this(new Object[] { arg0, arg1, arg2, arg3, arg4 });
	}

	public ZThread(Object[] args) {
		System.arraycopy(args, 0, this.args, 0, args.length);
	}
	
	public static void msleep(int msec) {
		try {
			Thread.sleep(msec);
		} catch (InterruptedException e) {
		}
	}
	
	public abstract void run();
}
