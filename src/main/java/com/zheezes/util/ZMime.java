package com.zheezes.util;

/**
 * parse mime
 * 
 * @author hcconquer@gmail.com
 */
public class ZMime {
	public static String getSuffix(String mime) {
		String[] dots = mime.split("/");
		if (dots.length < 2) {
			return null;
		}
		String suffix = dots[dots.length - 1].trim();
		for (int i = 0; i < suffix.length(); i++) {
			if (Character.isWhitespace(suffix.charAt(i))) {
				return null;
			}
		}
		return suffix;
	}
	
	public static void main(String[] args) {
		System.out.println(ZMime.getSuffix("Content Type: image/png"));
	}
}
