package com.zheezes.util;

public class ZBit {
	public static int bitget(int b, int o) {
		if (o < 0 || o > 7) {
			return -1;
		}
		b = b >> (7 - o);
		b = b & 0x01;
		return b;
	}
	
	public static int bitset(int b, int o) {
		if (o < 0 || o > 7) {
			return -1;
		}
		b = b & (0x01 << (7 - o));
		return b;
	}
}
