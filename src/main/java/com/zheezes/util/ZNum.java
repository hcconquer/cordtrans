package com.zheezes.util;

/**
 * @author hcconquer@gmail.com
 */
public class ZNum {
	public static int compare(int m, int n) {
		if (m < n) {
			return -1;
		} else if (m > n) {
			return 1;
		}
		return 0;
	}

	public static int max(int m, int n) {
		return m > n ? m : n;
	}

	public static int min(int m, int n) {
		return m < n ? m : n;
	}
	
	public static Integer parseString(String str, int radix) {
		try {
			return Integer.valueOf(str, radix);
		} catch (NumberFormatException e) {
		}
		return null;
	}
}
