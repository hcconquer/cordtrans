package com.zheezes.util;

/**
 * @author hcconquer@gmail.com
 */
public class ZCast {
	/**
	 * cast object to the type of the class
	 * 
	 * @param obj
	 *         object
	 * @param clazz
	 *         cast type class
	 * @return object with class type
	 */
	@SuppressWarnings("unchecked")
	public static <T> T cast(Object obj, Class<T> clazz) {
		if (obj == null || clazz == null) {
			return null;
		}
		if (!clazz.isInstance(obj)) {
			return null;
		}
		return (T) obj;
	}

	/**
	 * cast a Integer object to int value
	 * 
	 * @param value
	 *         int object
	 * @return int value or 0 on fail
	 */
	public static int cast(Integer value) {
		return value != null ? value.intValue() : 0;
	}
}
