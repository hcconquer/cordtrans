package com.zheezes.util;

import java.util.Iterator;
import java.util.List;

/**
 * @author hcconquer@gmail.com
 */
public class ZList {
	/**
	 * this method is thread safe but not sure remove as request
	 * when other do remove at same time
	 * 
	 * @param list
	 *         the list to operate
	 * @param begin
	 *         the begin to remove
	 * @param num
	 *         the num to remove
	 * @return < 0 if fail, the count removed
	 */
	public static <T> int remove(List<T> list, int begin, int num) {
		if (list == null || begin < 0 || num < 0
				|| (begin + num >= list.size())) {
			return -1;
		}
		Iterator<T> iter = list.iterator();
		while (begin-- > 0) {
			if (!iter.hasNext()) {
				return -1;
			}
			iter.next();
		}
		int count = 0;
		while (count < num) {
			if (!iter.hasNext()) {
				return -1;
			}
			iter.remove();
			iter.next();
			count++;
		}
		return count;
	}
}
