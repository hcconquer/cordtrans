package com.zheezes.util;

/**
 * @author hcconquer@gmail.com
 */
public class ZChar {
	/**
	 * check if a decimal num char
	 * 
	 * @param c
	 *         char
	 * @return true if is char from '0'-'9'
	 */
	public static boolean isDec(char c) {
		return ('0' <= c && c <= '9');
	}

	/**
	 * check if a hex num char
	 * 
	 * @param c
	 *         char
	 * @return true if is char from '0'-'9' 'a'-'f' 'A'-'F'
	 */
	public static boolean isHex(char c) {
		return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f')
				|| ('A' <= c && c <= 'F');
	}

//	/**
//	 * check if a decimal pair
//	 * 
//	 * @param cs
//	 *         a pair of char
//	 * @return true if a decimal pair
//	 */
//	public static boolean isDecPair(char[] cs) {
//		if (cs == null || cs.length != 2) {
//			return false;
//		}
//		return isDec(cs[0]) && isDec(cs[1]);
//	}
//
//	/**
//	 * check if a hex pair
//	 * 
//	 * @param cs
//	 *         a pair of char
//	 * @return true if a hex pair
//	 */
//	public static boolean isHexPair(char[] cs) {
//		if (cs == null || cs.length != 2) {
//			return false;
//		}
//		return isHex(cs[0]) && isHex(cs[1]);
//	}

	/**
	 * char('0') --> Integer(0)
	 * 
	 * @param c
	 *         char
	 * @return num or -1
	 */
	public static int toDec(char c) {
		if ('0' <= c && c <= '9') {
			return c - '0';
		}
		return -1;
	}
	
	/**
	 * Integer(0) --> char('0')
	 * 
	 * @param n
	 *         num
	 * @return char or '\0'
	 */
	public static char toDec(int n) {
		if (n < 0 || n > 10) {
			return '\0';
		}
		return (char) ('0' + n);
	}
	
	/**
	 * Integer(0) --> char('0')
	 * Integer(15) --> char('f')
	 * 
	 * @param n
	 *         num
	 * @return char if valid or '\0'
	 */
	public static char toHex(int n) {
		return toLHex(n);
	}
	
	/**
	 * Integer(0) --> char('0')
	 * Integer(15) --> char('f')
	 * 
	 * @param n
	 *         num
	 * @return char if valid or '\0'
	 */
	public static char toLHex(int n) {
		if (n < 0 || n > 0x0f) {
			return '\0';
		}
		return (char) (n < 10 ? ('0' + n) : ('a' + (n - 10)));
	}
	
	/**
	 * Integer(0) --> char('0')
	 * Integer(15) --> char('F')
	 * 
	 * @param n
	 *         num
	 * @return char if valid or '\0'
	 */
	public static char toUHex(int n) {
		if (n < 0 || n > 0x0F) {
			return '\0';
		}
		return (char) (n < 10 ? ('0' + n) : ('A' + (n - 10)));
	}

	/**
	 * char('0') --> Integer(0)
	 * char('f') --> Integer(15)
	 * char('F') --> Integer(15)
	 * 
	 * @param c
	 *         char 
	 * @return num if valid or -1
	 */
	public static int toHex(char c) {
		if ('0' <= c && c <= '9') {
			return c - '0';
		} else if ('a' <= c && c <= 'f') {
			return c - 'a' + 10;
		} else if ('A' <= c && c <= 'F') {
			return c - 'A' + 10;
		}
		return -1;
	}

	/**
	 * char[]{'1', '0'} --> Integer(10)
	 * 
	 * @param cs
	 *         decimal num char pair
	 * @return num or -1
	 */
	public static int toDec(char[] cs) {
		int td = toDec(cs[0]);
		int ud = toDec(cs[1]);
		if (td < 0 || ud < 0) {
			return -1;
		}
		return td * 10 + ud;
	}

	/**
	 * char[]{'1', 'f'} --> Integer(0x1f)
	 * 
	 * @param cs
	 *         hex num char pair
	 * @return num or -1
	 */
	public static int toHex(char[] cs) {
		int td = toHex(cs[0]);
		int ud = toHex(cs[1]);
		if (td < 0 || ud < 0) {
			return -1;
		}
		return td * 16 + ud;
	}
	
	/**
	 * Integer(12) --> char[]{'1', '2'}
	 */
	public static char[] toDecs(int num) {
		if (num < 0 || num > 99) {
			return null;
		}
		char[] pair = new char[2];
		pair[0] = toDec(num / 10);
		pair[1] = toDec(num % 10);
		return pair;
	}
	
	/**
	 * Integer(0x12) --> char[]{'1', '2'}
	 */
	public static char[] toHexs(int num) {
		if (num < 0 || num > 0xff) {
			return null;
		}
		char[] pair = new char[2];
		pair[0] = toHex(num / 16);
		pair[1] = toHex(num % 16);
		return pair;
	}
}
