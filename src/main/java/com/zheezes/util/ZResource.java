package com.zheezes.util;

import java.io.File;
import java.net.URL;

/**
 * @author hcconquer@gmail.com
 */
public class ZResource {
	/**
	 * get resources in the classpath
	 * 
	 * @param path
	 * @return
	 */
	public static URL get(String path) {
		return ZResource.class.getResource(path);
	}
	
	/**
	 * get current pwd
	 * 
	 * @return pwd path
	 */
	public static File dir() {
		return new File("./");
	}
	
	/**
	 * get file from class path or pwd
	 * 
	 * @param path
	 * @return file
	 */
	public static File getFile(String path) {
		URL url  = get(path);
		File file = null;
		if (url != null) {
			String fp = url.getFile();
			if (fp != null) {
				file = new File(fp);
			}
		} else {
			File dir = dir();
			file = new File(dir, path);
		}
		return file;
	}
}
