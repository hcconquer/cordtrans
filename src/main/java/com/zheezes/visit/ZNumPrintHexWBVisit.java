package com.zheezes.visit;

/**
 * @author hcconquer@gmail.com
 */
public class ZNumPrintHexWBVisit implements ZINumVisit {
	@Override
	public int visit(int c) {
		System.out.printf("%02x ", c);
		return 0;
	}
}
