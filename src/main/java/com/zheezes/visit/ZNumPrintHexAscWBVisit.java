package com.zheezes.visit;

/**
 * @author hcconquer@gmail.com
 */
public class ZNumPrintHexAscWBVisit implements ZINumVisit {
	@Override
	public int visit(int c) {
		System.out.printf("%02x ", c & 0xff);
		return 0;
	}
}
