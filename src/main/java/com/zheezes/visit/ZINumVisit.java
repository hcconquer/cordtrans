package com.zheezes.visit;

/**
 * @author hcconquer@gmail.com
 */
public interface ZINumVisit {
	public int visit(int c);
}
